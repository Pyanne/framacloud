# Installation de Minetest

## Description

![](images/minetest/minetest-screenshot.jpg)

[Minetest][2] est un logiciel libre distribué sous [LGPL v2.1+][3].
Il s’agit d’un jeu de type bac-à-sable en monde ouvert, inspiré de
InfiniMiner, Minecraft, et d’autres semblables.

Minetest est très facilement modulable. Il peut ainsi servir des
objectifs totalement différents, selon les *mods* qu’on y installe.
Une installation pourra servir comme jeu de ferme entre amis,
une autre installation comme d’un jeu avec des monstres à battre,
une autre installation comme d’un support éducatif et encore plein d’autres.
C’est le support éducatif qui a intéressé Framasoft.

<div class="alert alert-info">
  Ce guide est prévu pour Debian Stable. À l’heure où nous écrivons ces
  mots, il s’agit de Debian 8.5 Jessie. Nous partons du principe que
  vous venez d’installer le système et que celui-ci est à jour.
</div>

## Nourrir la terre

![](images/icons/preparer.png)

### Utilisateur dédié

De manière à mieux isoler l’application, nous allons créer un
utilisateur dédié à Minetest, du même nom que le logiciel :

    useradd -mU minetest

Cette commande crée l’utilisateur minetest ainsi qu’un groupe du même nom.
Il lui assigne comme répertoire personnel `/home/minetest`, que minetest
utilisera pour stocker ses données (monde, mods, textures, données utilisateurs…).

Nous allons aussi créer quelques dossiers qui nous serviront par la
suite pour installer les *mods* et les textures.

    mkdir -p /home/minetest/.minetest/textures /home/minetest/.minetest/worlds
    chown -R minetest: /home/minetest/.minetest/

## Semer

![](images/icons/semer.png)

### Minetest

Il suffit d’installer minetest-server, de préférence depuis les dépôts
`jessie-backports` pour avoir la dernière version disponible :

    echo "deb http://ftp.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list
    apt-get update
    apt-get install minetest-server -t jessie-backports

Précision sur l'ouverture des ports : le port dédié à Minetest habituellement
est le port `30000` en `udp` (mais vous pouvez choisir un autre port comme `30001`)

## Arroser

![](images/icons/arroser.png)

### Installer les mods

Téléchargez notre pack de texture, notre mod et notre exemple de configuration

    cd /home/minetest
    wget https://framinetest.org/dl/worldmods.tar.gz https://framinetest.org/dl/textures.tar.gz https://framinetest.org/dl/minetest.conf

Décompressez-les aux endroits idoines

    cd .minetest/textures
    tar xvf /home/minetest/textures.tar.gz
    cd ../worlds
    # Créez un dossier avec un nom qui vous convient, exemple `FraminetestEdu`
    mkdir FraminetestEdu
    cd FraminetestEdu
    tar xvf /home/minetest/worldmods.tar.gz

#### Liste des mods

Voici la listes des mods inclus dans notre pack spécial « éducation » :
**(EDIT : liste mise à jour au 15/12/2016)**

    3d_armor                               areas                                  moreblocks
    biome_lib-master                       moreores
    digilines-master                       display_modpack                        moretrees
    farming_plus                           nether
    freeze                                 pipeworks-master
    gloopblocks                            plantlife_modpack
    h2omes                                 quartz
    hbhunger                               homedecor_modpack                      serveressentials
    hudbars                                serverguide
    intllib                                signs_lib
    lavapriv                               streets-1.5
    mapfix                                 torches
    mapfix-old.zip                         unified_inventory
    minetest-mod-mesecons-master           worldedit
    xban2

### Configurer Minetest

Éditez la configuration selon vos préférences

    vi minetest.conf

### Changer le propriétaire

Comme jusque là, on utilisait l'utilisateur root pour travailler,
il est important de rendre à minetest ce qui est à minetest

    chown -R minetest: /home/minetest

Et pour que nous puissions utiliser le dossier de log du système

    chown -R minetest /var/log/minetest

Pour démarrer le serveur sur un monde spécifique, il suffit de faire :

    minetestserver --config /home/minetest/minetest.conf --worldname xxx

ou simplement, si un monde par défaut est configuré dans le `minetest.conf` :

    minetestserver --config /home/minetest/minetest.conf

Attention, assurez-vous d'abord que le serveur minetest
lancé à l'installation du paquet soit coupé :

    systemctl stop minetest-server

## Regarder pousser

![](images/icons/pailler.png)

### Créer un service pour Minetest

Un serveur minetest, c'est bien, mais c'est encore mieux
si le serveur se lance automatiquement au démarrage de la machine.

Tout d'abord, nous il faut couper et désactiver le serveur minetest
qui s'est lancé à l'installation du paquet :

    systemctl stop minetest-server
    systemctl disable minetest-server

Puis nous créons un service pour notre configuration :

    vi /etc/systemd/system/minetest.service

Dont le contenu sera :

    [Unit]
    Description=Minetest multiplayer server minetest.conf server config
    Documentation=man:minetestserver(6)
    After=network.target
    RequiresMountsFor=/home/minetest
    [Service]
    Restart=on-failure
    User=minetest
    Group=minetest
    ExecStart=/usr/lib/minetest/minetestserver --config /home/minetest/minetest.conf --logfile /var/log/minetest/minetest.log
    [Install]
    WantedBy=multi-user.target

On l'active pour qu'il se lance au démarrage et on le lance :

    systemctl daemon-reload
    systemctl enable minetest
    systemctl start minetest

Vous avez la possibilité de programmer des horaires d'ouverture/fermeture
via des tâches cron pour, par exemple, n'allumer le service que pendant
les heures de cours.

### Autres serveurs

Vous pouvez également trouver votre bonheur dans [l'un des serveurs publics][4]

### Alternative

**Pour des groupes de 4-8 élèves**

Il existe enfin une dernière possibilité, très simple à mettre en place,
mais limité à quelques élèves : utiliser le client Minetest en mode
graphique (comme si vous alliez jouer), mais en choisissant l'onglet "Server" (au lieu de client).
Une fois le serveur démarré de cette manière, il ne vous reste plus qu'à :

*   Vérifier que votre parefeu autorise le port `30000/UDP` en entrée ;
*   Vérifier que tous les joueurs sont sur le même réseau que vous ;
*   Distribuer votre adresse IP (celle de l'ordinateur faisant office de serveur).
    (Sous Windows, la commande `ipconfig` dans une invite de commande
    permet de l’avoir. Sur GNU/Linux, on utilisera la commande `ip addr show`.

* * *

Merci à Powi et Sangokuss pour la rédaction de ce tutoriel !

 [1]: https://framinetest.org
 [2]: http://www.minetest.net/
 [3]: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU
 [4]: http://www.minetest.net/servers/