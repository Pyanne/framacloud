# Installation de Framaestro

[Framaestro][1] est un logiciel développé par Framasoft qui permet d’agréger sur une même page web plusieurs services en ligne et notamment d’autres services Framasoft.

![][2]

Le principe de fonctionnement est simplissime : le créateur d’un « maestro » ajoute des iframes qui pointent vers les différents outils dont il a besoin. Chaque fenêtre peut être redimensionnée et repositionnée (elles sont gérées avec [jsPanel][3]). Une URL est automatiquement générée en fonction du contenu pour partager le maestro avec ses collaborateurs. Nous avons ajouté une version un peu personnalisée de [TogetherJS][4] – un outil développé par [Mozilla][5] – pour pouvoir proposer quelques options rudimentaires de communication et de partage.

En pratique, Framaestro n’enregistre aucune donnée sur nos serveurs (à part les logs de connexion ou de mise en relation des utilisateurs entre eux). Par contre, plusieurs outils sont dépendants de services tiers : Framatalk pour la visioconférence, Frama.link pour raccourcir les liens de partage, Framapad, -calc, -memo et -vectoriel proposés pour ajouter des documents vierges en un clic, notre hub TogetherJS, KiwiIRC, notre barre de navigation… donc il faudra un peu le personnaliser.

## Installation

![](images/icons/bonzai.png)

Framaestro est donc comme un bonsaï : on le pose quelque part et il pousse tout seul. L’installation sur un serveur est extrêmement simple puisqu’il ne stocke rien en ligne et n’est constitué que de CSS, HTML et JavaScript.

Donc, téléchargez les fichiers de la dernière version sur [notre dépôt git][6]

Décompressez l’archive en local et uploadez le tout sur votre serveur.

Pour utiliser le logiciel, il suffit de vous rendre sur `http://votre-site.org/framaestro/index.html`. Les maestros sont gérés depuis le fichier `/framaestro/p/index.html`.

## Tailler

![](images/icons/tailler.png)

### Framanav

Pour enlever la barre de navigation de Framasoft, vous devez supprimer dans le fichier `p/config.js` la ligne :

    framanav = 'https://framasoft.org/nav/nav.js';


### Frama.link

Les liens raccourcis sont générés en s’appuyant sur l'API de Frama.link. Dans la configuration de [LSTU][7] (le logiciel qui fait tourner Frama.link) seul le domaine framaestro.org est autorisé à s’en servir. Il vous faudra donc désactiver cette option ou bien installer votre propre instance de [LSTU][7].

Pour désactiver cette option, il suffit de supprimer la ligne :

    lstu = 'https://frama.link/a';


### Framaservices (facultatif)

La liste des applications proposées se trouve dans le fichier `p/config.js` avec des lignes du type :

    'calc' : { i: 'fa-th', u: 'https://framacalc.org/', t: 'Calc' },


`i` correspond à l'icône [Font-Awesome][8], `t` correspond au texte et `u` correspond au lien qui sera suffixé du nom du maestro ainsi que d’un numéro (selon le nombre d’iframes déjà présentes) avant de créer l’iframe. Ici, lorsqu’un utilisateur ajoute un calc, le lien du calc devient donc

    https://framacalc.org/12345678MonProjet1


Pour enlever cette section de l’ajout de fenêtres, vous pouvez supprimer dans le fichier `p/config.js` les lignes :

    framaServices = {
    …
    }


### Framatalk et KiwiIRC (facultatif)

Même chose, pour désactiver ou remplacer Framatalk et KiwiIRC avec les lignes :

    jitsi = 'https://framatalk.org';
    kiwi = 'https://kiwiirc.com/client/';


### TogetherJS (facultatif)

TogetherJS est utilisé pour la gestion du tchat, de la conférence audio, le suivi des mouvements de souris des utilisateurs et la synchronisation des fenêtres. Pour le désactiver, il suffit de supprimer la ligne :

    together = 'https://hub.framaestro.org';


Pour information, nous avons un peu adapté le logiciel de Mozilla pour l’intégrer au mieux à Framaestro (traduction française, mise en forme, fonctionnalités masqués ou réagencées). Normalement il ne devrait pas être nécessaire d’apporter des changements supplémentaires au « client » (fichiers `p/ressources/togetherjs.*.js` et dossier `p/ressources/togetherjs`).

Mais si vous le souhaitez, vous pouvez le recompiler à partir des sources officielles. Nous avons isolé nos changements dans le dossier `_src/togetherjs`.

#### Le client JavaScript (facultatif)

Tout d'abord installez NodeJS et Grunt **sur votre machine locale** (on présuppose que cette machine est sur Debian Jessie).

    apt-get install nodejs-legacy
    npm install -g grunt-cli


Puis clonez le dépôt officiel de TogetherJS dans un dossier de travail

    cd ~/votre_dossier
    git clone https://github.com/mozilla/togetherjs.git


Copiez selon vos besoins les fichiers du dossier `_src/togetherjs` de Framaestro à l'emplacement correspondant sur TogetherJS. Effectuez vos changements et recompilez le client en exécutant la commande ci-dessous :

    grunt build buildsite --no-hardlink


Vous n’avez ensuite qu’à copier les fichiers `build/togetherjs.*.js` et le dossier `build/togetherjs` dans `p/ressources` sur Framaestro.

#### Le serveur hub (facultatif)

<div class="alert alert-info">
  Cette partie du guide est prévue pour Debian Stable. À l’heure où nous écrivons ces mots, il s’agit de Debian 8.6 Jessie. Nous partons du principe que vous venez d’installer le système et qu’il est à jour. Dans la suite de ce tutoriel, nous supposerons que vous avez déjà fait pointer le nom de domaine sur votre serveur auprès de votre <a href="http://fr.wikipedia.org/wiki/Bureau_d%27enregistrement">registraire</a>
</div>

Pour que les utilisateurs puissent s’envoyer des instructions entre eux (messages, communications audios et actions JavaScript), la mise en relation s’effectue grâce à notre serveur. Le hub est très peu gourmand donc vous pouvez tout à fait utiliser le nôtre mais vous pourriez aussi avoir envie d’installer le vôtre.

Pour cela, il faut prévoir d’ajouter **un autre domaine `hub.votre-domaine.org` qui pointera vers un serveur dédié** et d’installer NodeJS.

C’est parti ! Installez NodeJS :

    apt-get install nodejs-legacy


Depuis le dossier `/var/www/`, clonez le dépôt officiel de TogetherJS dans un dossier `hub.votre-domaine` (en remplaçant « votre-domaine » évidement).

    cd /var/www
    git clone https://github.com/mozilla/togetherjs.git hub.votre-domaine


Dans ce dossier installez les paquets NodeJS nécessaires au fonctionnement du serveur hub.

    cd hub.votre-domaine
    npm install


Puis changez le propriétaire du dossier.

    cd ..
    chown -R www-data:www-data hub.votre-domaine


Créez également, s’il n’existe pas déjà, le dossier qui contiendra les logs du nom de domaine.

    mkdir /var/log/votre-domaine
    chown -R www-data:www-data /var/log/votre-domaine


<div class="alert alert-info">
  À cette étape il suffit de lancer <code>node hub/server.js</code> pour avoir un hub fonctionnel sur le port <code>8080</code>. Il est possible de changer le port, le niveau de log, etc, la liste des options est disponible sur <code>node hub/server.js --help</code>.
</div>

Pour configurer le serveur hub comme un service afin qu’il puisse être lancé au démarrage de la machine ou à l’aide de la commande `service hub.votre-domaine start`, créez le fichier `/etc/systemd/system/hub.votre-domaine.service` contenant ceci :

    [Unit]
    After=systemd-user-sessions.service

    [Service]
    User=www-data
    ExecStart=/usr/bin/node /var/www/hub.votre-domaine/hub/server.js --log=/var/log/framaestro/hub.votre-domaine.log --log-level=2
    Restart=always

    [Install]
    WantedBy=multi-user.target


Vous pouvez démarrer le serveur hub `service hub.votre-domaine start` et faire en sorte qu’il soit lancé automatiquement lors d’un redémarrage de la machine : `systemctl enable hub.votre-domaine.service`

Nous allons maintenant rendre le hub accessible depuis un nom de domaine avec Nginx sur une connexion HTTP chiffrée en utilisant un certificat <i lang="en">Let’s encrypt</i>.

##### Nginx

En tant que `root`, installez le paquet nginx : `apt-get install nginx` s’il n’y est pas déjà

Créez le fichier `/etc/nginx/sites-available/hub.votre-domaine` pour y mettre ceci. Les textes précédés d’un # sont des commentaires pour vous expliquer à quoi servent chaque partie de la configuration. Certaines parties du code sont aussi « commentées » dans le but de les activer plus tard lorsque les certificats auront été créés (cf partie <i lang="en">Let’s encrypt</i> ci-dessous) :

    server {
        listen *:80 ;           # le site sera accessible sur le port 80, c'est à dire en HTTP, avec l'adresse IPv4
        listen [::]:80 ;        # … également avec l'adresse IPv6

        #listen *:443 ssl ;     # … ainsi qu'en HTTPS (port 443)
        #listen [::]:443 ssl ;

        server_name hub.votre-domaine.org ;

        access_log /var/log/nginx/hub.votre-domaine.access.log ; # emplacement où sont enregistrés les logs de visite
        error_log /var/log/nginx/hub.votre-domaine.error.log ;   # … et d'erreur

        # Certificats SSL
        #ssl_certificate /etc/letsencrypt/live/hub.votre-domaine.org/fullchain.pem ;
        #ssl_certificate_key /etc/letsencrypt/live/hub.votre-domaine.org/privkey.pem ;

        #ssl_session_timeout 5m ;
        #ssl_session_cache shared:SSL:5m ;

        add_header Strict-Transport-Security max-age=15768000 ;

        # Redirection Proxy vers le hub TogetherJS
        location / {
            include /etc/nginx/proxy_params;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_set_header        X-Forwarded-Proto https;
            proxy_pass  http://127.0.0.1:8080 ;
        }

        # On oblige le visiteur à consulter le site en HTTPS
        #if ($server_port = 80) {
        #    rewrite (.*) https://$http_host$1 permanent ;
        #}

        # Emplacement réservé à la validation des certificats SSL par certbot
        location ^~ '/.well-known/acme-challenge' {
            default_type "text/plain" ;
            root /var/www/certbot ;
        }
    }


Activez le site :

<pre>ln -s /etc/nginx/sites-available/hub.votre-domaine /etc/nginx/sites-enabled/hub.votre-domaine</pre>

Enfin, relancez nginx : `nginx -t && service nginx reload`

##### Let's encrypt

Installez le client `certbot` qui permet de générer les certificats de manière automatisée. Il est disponible dans les dépôts `jessie-backports` :

<pre>echo "deb http://ftp.debian.org/debian jessie-backports main" > /etc/apt/sources.list.d/backports.list
apt-get update
apt-get install certbot -t jessie-backports</pre>

Dans `/etc/cron.d/certbot` se trouve la tâche planifiée qui renouvellera automatiquement les certificats. Pour que ce renouvellement soit pris en compte par Nginx, ajoutez `--post-hook "/usr/sbin/nginx -s reload"` au bout de la commande (après `certbot -q renew`).

Créez le dossier `/var/www/certbot` puis créez le certificat :

<pre>certbot certonly --rsa-key-size 4096 --webroot -w /var/www/certbot/ -d hub.votre-domaine.org</pre>

`-d` indique le nom de domaine pour lequel le certificat sera valide. Il faut bien évidemment que les enregistrements DNS de votre registraire soient valides et qu'ils pointent bien vers le serveur où on lance la commande certbot.

`-w` indique la racine du site, là où certbot peut poser ses fichiers pour la vérification.

Les certificats seront disponibles dans `/etc/letsencrypt/live/hub.votre-domaine.org` :

*   `cert.pem` : le certificat tout seul
*   `chain.pem` : la chaîne de certification
*   `fullchain.pem` : le certificat concaténé à la chaîne de certification
*   `privkey.pem` : la clé du certificat

Une fois les certificats créés, dé-commentez les lignes `listen […] 443 ssl`, la section « Certificat SSL » et la section « On oblige le visiteur à consulter le site en HTTPS » dans la configuration Nginx puis relancez nginx : `nginx -t && service nginx reload`

Voilà normalement le hub est fonctionnel et vous pouvez remplacer l’url dans le fichier `config.js` de Framaestro.

<p class="text-center">
  <span class="fa fa-leaf" aria-hidden="true" style="font-size:200px; color:#333"></span>
</p>

 [1]: https://framaestro.org
 [2]: images/framaestro/slide-framaestro02.jpg
 [3]: http://jspanel.de/
 [4]: http://togetherjs.com/
 [5]: http://mozilla.org
 [6]: https://framagit.org/framasoft/framaestro/
 [7]: cultiver-son-jardin/lstu.html
 [8]: http://fontawesome.io/
