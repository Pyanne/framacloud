Pré-requis
==========

Vous êtes décidés à tenter l’aventure? Alors c’est parti. Voyons voir de
quoi nous allons avoir besoin :

Le matériel
-----------

Du point de vue matériel, plusieurs choix s’offrent à vous :

-   Récupérer de l’ancien matériel. Par exemple, le vieil ordinateur qui
    traîne dans un coin. Ou bien le portable à l’écran cassé. C’est le
    moins onéreux, et c’est écologique.
-   Acheter une carte comme le [raspberry pi](http://raspberrypi.org)
    qui est bon marché et consomme très peu.
-   Acheter les pièces pour concevoir un serveur basse consommation. Là,
    c’est vraiment si vous avez déjà de bonnes connaissances.

Vous pouvez jeter un œil au paragraphe [hw] pour des
exemples de configuration.

Le système d’exploitation
-------------------------

Vous êtes libres d’installer le système d’exploitation qui vous convient
le mieux. Cependant, par souci de simplicité, on supposera dans la suite
du document que vous utilisez [debian](http://debian.org), une
distribution GNU/Linux souvent conseillée.

Pourquoi?

Debian est stable, simple à mettre à jour, facile à installer. Niveau
sécurité, en cas de faille découverte dans un logiciel, les mises à jour
avec correctifs sont très rapidement disponibles. Enfin, en plus des
pages de manuel habituelles, de la documentation et des exemples sont le
plus souvent fournis dans le répertoire
`/usr/share/doc/NOM-DU-LOGICIEL`.

Notez qu’il est tout à fait possible d’utiliser les mêmes instructions
pour une autre distribution. Seule l’installation des paquets et des
dépendances sera différente. Cherchez dans ce cas l’équivalent
d’*apt-get* pour votre distribution.

Un nom de domaine
-----------------

Vous voudrez certainement obtenir un nom de domaine, qui permettra à
tous de retrouver simplement votre serveur. Cela vous permet aussi de
mieux vous organiser avec des sous-domaines, par exemple
`mail.mondomaine.com`, `blog.mondomaine.com`…

<div class="alert alert-info">
M. Ali Gator vit au 5 rue du moulin à Picsouville. Pour
aller lui rendre visite, c’est à cette adresse que vous allez vous
rendre.

Sur l’internet, l’adresse de votre serveur, c’est une série de
nombres. Par exemple <em>93.22.160.7</em>. C’est pratique pour les
machines, pas pour les humains.

Un nom de domaine permet aux humains
d’utiliser l’adresse <em>wikipedia.org</em>, qui est traduit par les
ordinateurs en <em>91.198.174.192</em>.

Avouez que c’est plus facile à retenir.
</div>

<div class="alert alert-info">
<p>Les séries de nombres indiquant l'adresse d'un serveur est ce qu'on appelle une adresse ip.</p>
<p>L’association d’une ip avec un nom de domaine est possible grâce aux <abbr title="Domain Name System">DNS</abbr></p>
</div>

### Achat d’un nom de domaine

Vous pouvez acheter un nom de domaine auprès de ce que l’on appelle un
*registre* ou *registrar*. En voici quelques exemples :

-   [OVH](http://www.ovh.com/fr/index.xml)
-   [Gandi](http://www.gandi.net)
-   [bookmyname](https://www.bookmyname.com)

Vous allez simplement pouvoir vérifier que le nom de domaine que vous
souhaitez acheter est disponible (choisissez-en un qui vous correspond).
Si oui, alors vous passez une commande comme sur n’importe quel site
marchand. C’est très rapide, et la plupart du temps, votre adresse ip
est déjà par défaut associée à votre nom de domaine.

### Nom de domaine gratuit

Certains registres proposent des noms de domaine, avec moins de
flexibilité pour vous cependant. Cela peut être intéressant le temps de
se faire la main. Quelques exemples :

-   [FDN](http://www.fdn.fr/)
-   [EU.org](http://eu.org/)
-   [FreeDNS](http://freedns.afraid.org/)

### Relier mon nom de domaine à mon adresse ip

Une fois que vous avez un nom de domaine, il faut le relier à l’adresse
ip de votre serveur. Mais si, souvenez vous, cette série de nombres
ressemblant à *91.198.174.192*.

Pour cela, enregistrez un champ de type **A** dans l’interface
d’administration du registre. Par exemple :

  mondomaine.com    A    34.121.124.123

Comment connaître mon adresse ip me direz-vous? Rien de plus simple, il
existe de nombreux services qui vous permettent de la retrouver.
Quelques exemples :

-   [http://lehollandaisvolant.net/tout/ip](http://lehollandaisvolant.net/tout/ip/)
-   <http://who.is/>
-   <http://www.whatsmyip.org/>

<div class="alert alert-warning">
<p>Il existe plusieurs types d’enregistrements :</p>
<ul>
    <li>Les champs <b>A</b> pour les adresses IPv4. ex : <code>12.123.123.12</code></li>
    <li>Les champs <b>AAAA</b> pour les adresses IPv6. ex : <code>2001:db8:4212:4212:4212:4212:4212:4212</code></li>
    <li>Les champs <b>MX</b>, <b>NS</b> et <b>CNAME</b>.<br>
        Ces derniers seront particulièrement utiles pour définir
        des sous-domaines.<br>
        En effet, vous pouvez faire pointer un sous-domaine vers le domaine
        principal avec un CNAME au lien d'enregistrer un nouveau
        champ A. Exemple :<br>
        <pre>blog.mondomaine.com    CNAME    mondomaine.com</pre>
        Cela permet d'organiser plusieurs sites sur son serveur
        proprement.</li>
</ul>
</div>

Apprenez à rediriger les ports de votre *box
---------------------------------------------

Votre serveur, tout comme votre ordinateur actuellement, sera
certainement connecté à internet par l’intermédiaire d’un modem (une
\*box).

Il faut s’assurer que lorsqu’un visiteur voudra accéder à votre serveur,
la \*box le redirige bien vers votre serveur, et non vers une autre
machine du réseau local. On dit que l’on *configure le routeur*.

<div class="alert alert-info">
<p>Imaginez votre *box comme un grand mur avec dedans plusieurs portes.
Chaque porte est numérotée.</p>
<p>Quand quelqu'un veut accéder à votre serveur,
il va venir frapper à l'une des portes, par exemple la numéro
80 pour un serveur http. Afin que tout fonctionne bien, il est nécessaire
de savoir où mène la porte n°80. Si la box ne le sait pas, alors
la porte 80 reste fermée et votre serveur est inaccessible.</p>

<p>Bien sûr, pour plus de sécurité encore, une fois la porte 80 passée,
votre serveur sera équipé d'un parefeu pour vérifier que vous avez
bien le droit d'entrer.</p>

<p class="text-center"><img src="images/ports.png" alt="Schéma de redirection des ports"></p>

<p>Dans le schéma ci-dessus, seuls les ports 443, 80 et 22 sont associées au serveur.</p>

<p>Si le petit malin demande un port qui n'est pas associé au serveur
(la porte est fermée), alors la requête ne peut pas aller
jusqu'au bout. C'est comme s'il demandait à aller à
une destination qui n'existe pas.</p>

<p>En revanche, lorsque le visiteur demande à passer par la porte
80, il est bien renvoyé vers le serveur.</p>
</div>

Cela se configure toujours de la même façon :

1.  Vous accédez à l’interface de configuration du modem
2.  Vous préciser le port d’écoute par lequel vont arriver les requêtes.
    Par exemple, le port 80 pour un site web.
3.  Vous indiquez que ce qui est adressé à ce port doit être mis en
    relation avec le port 80 de votre serveur (et pas un autre
    ordinateur connecté à la \*box).

Cependant, l’interface de configuration n’est pas la même selon si vous
avez une livebox, freebox, modem OVH…  Pas d’inquiétude, on peut trouver
l’adresse à taper dans un navigateur web pour accéder à cette interface.
Essayez dans l’ordre suivant :

-   [192.168.0.1](192.168.0.1)
-   [192.168.1.1](192.168.1.1)
-   [192.168.1.254](192.168.1.254)
-   Pour une freebox, cela se passe sur la page de gestion de votre
    compte

Bien sûr, cette “adresse” est à utiliser sur un ordinateur lui-même
connecté à la \*box.

Il est possible qu’un nom d’utilisateur et un mot de passe soient
demandés. Essayez dans ce cas |admin/admin|, sinon, demandez directement
à votre fournisseur d’accès à internet. Une fois connecté, allez dans la
section “Configurer mon routeur”.

Pour plus d’informations, cette page peut vous être utile :
<https://craym.eu/tutoriels/utilitaires/ouvrir_les_ports_de_sa_box.html>.

Comment éditer un fichier?
--------------------------

Dans la suite du document, on vous parlera souvent d’*éditer un
fichier*. Ça veut simplement dire qu’il faudra modifier du texte dans ce
fichier.

Sous linux, il existe une ribambelle d’éditeurs de texte (vim, nano…).
Si vous débutez, le plus simple sera certainement nano.

Prenons un exemple. Pour éditer le fichier `/etc/iloverocknroll`, vous
taperez ceci :

```
nano /etc/iloverocknroll
```

Apparaîtra alors le contenu de ce fichier dans le terminal :

![image](images/iloverocknroll.png)

Remarquez que en bas de la fenêtre, il y a quelques rappels sur les
raccourcis. Le symbole `^` signifie qu’il faut appuyer sur la touche
*Ctrl* (en bas à gauche du clavier).

Une fois votre document modifié, vous allez donc quitter en appuyant
simultanément sur ctrl et X. On vous demandera si vous souhaitez
enregistrer vos modifications. Taper “O” puis validez avec Entrée.

